<?php

include_once 'core/autoloader.php';

use Core\Init as Init;
use Controllers\ViewController as ViewController;
use Controllers\LoginController as LoginController;
use Controllers\UserController as UserController;

$app = new Init();
$viewController = new ViewController();
$loginController = new LoginController();
$userController = new UserController();

$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

// Route it up!
switch ($request_uri[0]) {

    // Home page
    case '/':
        $viewController->index();
        die();
        break;

    // Login page
    case '/login':
            $loginController->redirect();
        die();
        break;

    // Login page
    case '/logout':
        $viewController->logout();
        die();
        break;

    // Redirect page
    case '/redirect':
        if(isset($_GET["code"])){
            $loginController->checkToken();
            die();
        }elseif(isset($_GET["error_code"])) {
            //header('HTTP/1.0 404 Not Found');
            $viewController->errorPage();
            die();
        }
        break;

    // Login page
    case '/user':
            $userController->showUserInformation();
        die();
        break;

    // Everything else
    default:
        //header('HTTP/1.0 404 Not Found');
        $viewController->errorPage();
        die();
        break;
}