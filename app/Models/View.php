<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 18-5-2018
 * Time: 19:53
 *
 * OOP Class for constructing HTML5 pages.
 * Constructor behandeld begin van de pagina,
 * Destructor het einde.
 */

namespace Models;


class View
{
    protected   $charset       = 'UTF8';
    protected   $lang       = 'nl-NL';
    private     $pageTitle  = 'Example.com';

    public function __construct($title = NULL)
    {
        if(isset($title))
        {
            $this->pageTitle = trim($title);
        }

        /*
         * HTTP-Compression
         */
        ob_start('ob_gzhandler');
        header('Content-Language: ' .$this->lang);
        header('Content-Type: text/html; charset='. $this->charset);


        /*
         * Begin <HTML>
         */
        echo    '<!DOCTYPE html>',
                '<html lang="'. $this->lang .'" >';

        /*
         * Container <head>
         */

        echo '<head>',
            '<meta http-equiv="Content-Language" content="text/html" charset="'.$this->charset.'">',"\n",
            '<meta charset="'.$this->charset.'">',"\n",
            '<meta name="description" content="">',"\n",
            '<meta http-equiv="x-ua-compatible" content="ie=edge">',"\n",
            '<meta name="viewport" content="width=device-width, initial-scale=1">',"\n",
            '<meta name="robots" content="noindex,nofollow">',"\n",
                '<title>'.$this->pageTitle.'</title>',"\n",
            '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">',"\n",
            '<link href="Views\css\style.css" rel="stylesheet" type="text/css">',"\n",
        '</head>',"\n";


        /*
         * Begin <Body>
         */
        echo '<body lang="'.$this->lang.'">',"\n";

        ob_flush();
        flush();

    }

    public function __destruct()
    {
        /*
         * General scripts to be loaded
         */

        echo
            '<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>',"\n",
            '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" crossorigin="anonymous"></script>',"\n",
            '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" crossorigin="anonymous"></script>',"\n";

        /*
         * End of <body>
         */

        echo '</body>',"\n",
        '</html>',"\n";

        ob_flush();
        flush();
    }


}