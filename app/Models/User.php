<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 18-5-2018
 * Time: 23:19
 */

namespace Models;


class User
{
    private $id, $name, $birthday, $pictures;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param $array
     */
    public function setPictures($array)
    {
        foreach($array as $key => $value)
        {
            $pictures[$key] = $value;
        }

        $this->pictures = $pictures;
    }

    /**
     * @return array
     */
    public function getPictures()
    {
        return $this->pictures;
    }
}