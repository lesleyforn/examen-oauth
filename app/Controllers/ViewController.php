<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 18-5-2018
 * Time: 21:21
 */

namespace Controllers;


class ViewController
{
    public function index()
    {
        $message = 'Please login in using facebook:';
        include_once "Views/home.phtml";
    }

    public function logout()
    {
        session_destroy();
        $message = 'You are logged out of this website';
        include_once "Views/home.phtml";
    }

    public function errorPage()
    {
        $message = 'Something went wrong... We apologise for this discomfort';
        include_once "Views/404.phtml";
    }
}