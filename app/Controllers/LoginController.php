<?php

namespace Controllers;

use Core\HelperFunctions;
use Models\User;

class LoginController
{
    public function redirect()
    {
        header("Location: https://www.facebook.com/dialog/oauth?client_id=1940876716225618&redirect_uri=https://localhost.oauth.com/redirect&scope=email,user_birthday");
        return null;
    }

    public function logout()
    {

    }

    public function checkToken(){
        $functions = new HelperFunctions();

        $token_and_expire = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=1940876716225618&redirect_uri=https://localhost.oauth.com/redirect&client_secret=b5d32f9232c7118f4790f81f8a9f63be&code=" . $_GET["code"]);
        $token_and_expire = $functions->removeAccessiveCharactes($token_and_expire);
        $_token_and_expire_array = $functions->objectCreator($token_and_expire);

        if(isset($_token_and_expire_array["access_token"]))
        {

            $access_token = $_token_and_expire_array["access_token"];

            if(isset($access_token)) {
                $user_information = file_get_contents("https://graph.facebook.com/v3.0/me?access_token=" . $access_token . "&fields=id,name,picture,birthday");
                $user_information_array = json_decode($user_information, true);

                $user = new User;

                $user->setId($user_information_array['id']);
                $user->setName($user_information_array['name']);
                $user->setBirthday($user_information_array['birthday']);
                $user->setPictures($user_information_array['picture']);


                $_SESSION['user'] = $user;
                return header("Location: https://localhost.oauth.com/user");
            }
        }

        return header("Location: https://localhost.oauth.com/redirect?error_code=Error-accorded");
    }
}

