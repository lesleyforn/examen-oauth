<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 18-5-2018
 * Time: 22:57
 */

namespace Controllers;


class UserController
{
    public function showUserInformation()
    {
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            include_once "Views/user.phtml";
            die();
        }

        header("Location: https://localhost.oauth.com/logout");
        die();
    }
}