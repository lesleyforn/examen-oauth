<?php

namespace Core;


class Init
{
    function __construct()
    {

        session_start();

        $URL = substr($_SERVER['REQUEST_URI'], 19);
        $URLElements = explode('/', $URL); // Adjust if needed.

        $class = $URLElements[0];
        $method = $URLElements[1];

        if (($t = substr_count($URL, '/')) > 1) {
            for ($i = 2; $i < $t + 1; $i++) {
                echo $URLElements[$i] . '<br />';
            }
        }
    }
}