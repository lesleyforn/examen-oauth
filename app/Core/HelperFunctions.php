<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 18-5-2018
 * Time: 21:44
 */

namespace Core;


class HelperFunctions
{
    public static function removeAccessiveCharactes($input)
    {
        return substr($input, 1, -1);
    }

    public function objectCreator(string $string)
    {
        list($access_token, $token_type, $expires_in) = explode(",",$string);

        list($access_token_key, $access_token_value) = explode(":",$access_token);
        list($token_type_key, $token_type_value) = explode(":",$token_type);
        list($expires_in_key, $expires_in_value) = explode(":",$expires_in);

        $array = array(
            $this->removeAccessiveCharactes($access_token_key) => $this->removeAccessiveCharactes($access_token_value),
            $this->removeAccessiveCharactes($token_type_key) => $this->removeAccessiveCharactes($token_type_value),
            $this->removeAccessiveCharactes($expires_in_key) => $expires_in_value
        );

        return $array;
    }
}