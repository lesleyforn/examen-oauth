<?php

/**
 * Standard PHP Library:
 * Autoload function for classes
 */
function my_autoloader($class)
{
    $filename = '/srv/app/' . str_replace('\\', '/', $class) . '.php';
    include($filename);
}

spl_autoload_register('my_autoloader');
